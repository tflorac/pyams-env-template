#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess

from grp import getgrnam
from pwd import getpwnam


#
# Replace "$((INSTALL))" by install directory for all files
#

TARGET = os.getcwd()

for root, dirs, files in os.walk(TARGET):
    for filename in files:
        # read file content
        with open(os.path.join(root, filename)) as f:
            content = f.read()
        # replace tag by installation path
        content = content.replace('$((INSTALL))', TARGET)
        # replace file content
        with open(os.path.join(root, filename), 'w') as f:
            f.write(content)


#
# Check for logs directory
#

user = '{{ cookiecutter.run_user }}'
user_id = getpwnam(user).pw_uid

group = '{{ cookiecutter.run_group }}'
group_id = getgrnam(group).gr_gid


DATA_DIRECTORY = '{{ cookiecutter.data_directory }}'.replace('$((INSTALL))', TARGET)
LOGS_DIRECTORY = '{{ cookiecutter.logs_directory }}'.replace('$((INSTALL))', TARGET)
TEMP_DIRECTORY = '{{ cookiecutter.temp_directory }}'.replace('$((INSTALL))', TARGET)


for directory in (DATA_DIRECTORY, LOGS_DIRECTORY, TEMP_DIRECTORY):
    if not os.path.exists(directory):
        try:
            os.makedirs(directory, mode=0o775, exist_ok=True)
        except PermissionError:
            print("WARNING: Can't create logs directory {0}".format(directory))
        else:
            try:
                os.chown(directory, user_id, group_id)
                os.chmod(directory, mode=0o775)
            except PermissionError:
                print("WARNING: Can't update permission on logs directory {0}".format(directory))

    for root, dirs, files in os.walk(directory):
        for dirname in dirs:
            try:
                target = os.path.join(directory, root, dirname)
                os.chown(target, user_id, group_id)
                os.chmod(target, mode=0o775)
            except PermissionError:
                print("WARNING: Can't set permissions for directory {0}".format(target))
        for filename in files:
            try:
                target = os.path.join(directory, root, filename)
                os.chown(target, user_id, group_id)
                os.chmod(target, mode=0o664)
            except PermissionError:
                print("WARNING: Can't set permissions for file {0}".format(target))


{%- if cookiecutter.use_elasticsearch %}
#
# Check Elasticsearch index
#

curl = shutil.which('curl')
have_curl = bool(curl)

{%- if cookiecutter.create_elasticsearch_index %}
if have_curl:
    json_dir = os.path.join(TARGET, 'docs', 'elasticsearch')
    try:
        subprocess.call(['curl', '-XPUT',
                         '{{ cookiecutter.elasticsearch_server }}/_ingest/pipeline/attachment',
                         '-d', '@' + os.path.join(json_dir, 'attachment-pipeline.json')])
        subprocess.call(['curl', '-XPUT',
                         '{{ cookiecutter.elasticsearch_server }}/_index_template/{{ cookiecutter.elasticsearch_index }}',
                         '-d', '@' + os.path.join(json_dir, 'template.json')])
        print("WARNING: You must install Elasticsearch ingest-attachment plug-in!")
    except:
        print("ERROR: Can't initialize ElasticSearch index. Please install CURL and read documentation!")
else:
    print("WARNING: You must install CURL to be able to initialize ElasticSearch index!")
{%- endif %}
{%- endif %}


print("\nYour server environment is initialized.")
print("To finalize it's creation, just type:")
print("- cd {{ cookiecutter.project_slug }}")
print("- apt install python3-pip")
print("- pip3 install --upgrade pip")
print("- pip3 install zc.buildout")
{%- if cookiecutter.use_oracle %}
print("- export ORACLE_HOME=/path/to/your/oracle/client")
{%- endif %}
{%- if cookiecutter.need_pyams_gis %}
print("- apt-get install libgdal-dev")
print("- export CPLUS_INCLUDE_PATH=/usr/include/gdal")
print("- export C_INCLUDE_PATH=/usr/include/gdal")
{%- endif %}
print("- buildout")
print("")
print("To run your development environment:")
print("- ./bin/pserve etc/development.ini")
