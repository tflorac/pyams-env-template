###
### Standard Pyramid configuration
###

[app:main]
use = egg:{{ cookiecutter.webapp_name }}

pyramid.reload_templates = true
pyramid.debug_templates = true
pyramid.debug_authorization = false
pyramid.debug_notfound = false
pyramid.debug_routematch = false
pyramid.default_locale_name = {{ cookiecutter.default_language }}
pyramid.includes =
    pyramid_zcml
    pyramid_debugtoolbar
    pyramid_beaker
    pyramid_chameleon
    pyramid_fanstatic
    pyramid_zodbconn
    pyramid_tm
    pyramid_retry
    pyramid_rpc.jsonrpc
    pyramid_rpc.xmlrpc
    cornice
    cornice_swagger
{%- if cookiecutter.use_elasticsearch_apm %}
    pyams_apm
{%- endif %}
    pyams_utils
    pyams_template
    pyams_viewlet
    pyams_pagelet
    pyams_mail
    pyams_site
    pyams_i18n
    pyams_catalog
    pyams_file
    pyams_security
    pyams_layer
    pyams_batching
    pyams_table
    pyams_form
    pyams_skin
    pyams_form
    pyams_file
    pyams_i18n
    pyams_zmi
    pyams_zodb_browser
    pyams_i18n_views
    pyams_file_views
    pyams_security_views
{%- if cookiecutter.use_apikey_auth %}
    pyams_auth_apikey
{%- endif %}
{%- if cookiecutter.use_azure_auth %}
    pyams_auth_azure
{%- endif %}
{%- if cookiecutter.use_http_auth %}
    pyams_auth_http
{%- endif %}
{%- if cookiecutter.use_jwt_auth %}
    pyams_auth_jwt
{%- endif %}
{%- if cookiecutter.use_ldap_auth %}
    pyams_auth_ldap
{%- endif %}
{%- if cookiecutter.use_oauth_auth %}
    pyams_auth_oauth
{%- endif %}
{%- if cookiecutter.use_sql_auth %}
    pyams_auth_sql
{%- endif %}
    pyams_fields
{%- if cookiecutter.use_scheduler %}
    pyams_zmq
    pyams_scheduler
{%- endif %}
    pyams_alchemy
{%- if cookiecutter.use_elasticsearch %}
    pyams_elastic
{%- endif %}
    pyams_workflow
    pyams_sequence
    pyams_portal
    pyams_thesaurus
{%- if cookiecutter.use_chat %}
    pyams_chat
{%- endif %}
{%- if cookiecutter.provide_content_cms %}
{%- if cookiecutter.use_elasticsearch %}
    pyams_content_es
{% else %}
    pyams_content
{% endif %}
    pyams_content_themes
{%- if cookiecutter.provide_content_api %}
    pyams_content_api
{%- endif %}
{%- endif %}
{%- if cookiecutter.extension_package %}
    {{ cookiecutter.extension_package }}
{%- endif %}

retry.attempts = 3
tm.annotate_user = false
zodbconn.uri = zconfig://%(here)s/zodb-{{ cookiecutter.db_type }}.conf

## By default, the toolbar only appears for clients from IP addresses
## '127.0.0.1' and '::1'.
debugtoolbar.hosts = 127.0.0.1 ::1

## Fanstatic settings
fanstatic.use_application_uri = true
fanstatic.publisher_signature = --static--
fanstatic.versioning = true
fanstatic.minified = false
fanstatic.bottom = true
fanstatic.debug = true

{%- if cookiecutter.use_elasticsearch_apm %}
## ElasticSearch APM settings
elasticapm.name = Elastic APM
elasticapm.server_url = {{ cookiecutter.apm_server }}
elasticapm.service_name = {{ cookiecutter.apm_service_name }}
elasticapm.service_version = {{ cookiecutter.apm_service_version }}
elasticapm.environment = {{ cookiecutter.apm_environment }}
elasticapm.debug = false

{%- endif %}
## Beaker sessions settings
beaker.session.type = {{ cookiecutter.beaker_backend }}
beaker.session.url = {{ cookiecutter.beaker_server }}
{%- if cookiecutter.beaker_backend == 'ext:redis' %}
beaker.session.db = 1
{%- endif %}
beaker.session.data_dir = {{ cookiecutter.data_directory }}/cache
beaker.session.lock_dir = {{ cookiecutter.data_directory }}/locks

## Beaker cache settings
beaker.cache.type = {{ cookiecutter.beaker_backend }}
beaker.cache.url = {{ cookiecutter.beaker_server }}
beaker.cache.data_dir = {{ cookiecutter.data_directory }}/cache
beaker.cache.lock_dir = {{ cookiecutter.data_directory }}/locks
beaker.cache.regions = short, default, long, persistent, views, portlets
{%- if cookiecutter.beaker_backend == 'ext:redis' %}
beaker.cache.short.db = 2
beaker.cache.default.db = 3
beaker.cache.long.db = 4
beaker.cache.persistent.db = 5
beaker.cache.views.db = 6
beaker.cache.portlets.db = 7
{%- endif %}
beaker.cache.short.expire = 60
beaker.cache.default.expire = 300
beaker.cache.long.expire = 3600
beaker.cache.persistent.expire = 604800
beaker.cache.views.expire = 600
beaker.cache.portlets.expire = 600


###
### PyAMS custom settings
###

## PyAMS application factory
## Default application factory is defined into PyAMS via a registered utility
## You can override this factory by defining this setting or by providing your own utility
# pyams.application_factory = pyams_content.root.SiteRoot

## PyAMS database root name in ZODB
pyams.application_name = {{ cookiecutter.project_slug }}

## PyAMS locale settings
pyams.locale.lc_ctype = {{ cookiecutter.pyams_locale }}
pyams.locale.lc_collate = {{ cookiecutter.pyams_locale }}
pyams.locale.lc_ctime = {{ cookiecutter.pyams_locale }}
pyams.locale.lc_monetary = {{ cookiecutter.pyams_locale }}
pyams.locale.lc_numeric = {{ cookiecutter.pyams_locale }}
pyams.locale.lc_all = {{ cookiecutter.pyams_locale }}

## PyAMS security settings
# Secure PyAMS authentication policy (requires HTTPS)
pyams.security_policy.secret = {{ cookiecutter.project_slug }}__123456
pyams.security_policy.secure = {{ cookiecutter.use_https }}

# Secure PyAMS cookie storage policy (requires HTTPS)
pyams.cookie_storage_policy.secure = {{ cookiecutter.use_https }}

# Disable PyAMS default security policy on site root
pyams.security.disable_default_policy = false

# Disable usage of FORBIDDEN_PERMISSION to system admin
pyams.security.deny_forbidden_to_admin = true

## PyAMS mailer
pyams_mail.mailers = debug. {{ cookiecutter.smtp_server_name }}.
debug.name = debug
debug.mode = debug
debug.top_level_directory = {{ cookiecutter.data_directory }}/mail/debug
{{ cookiecutter.smtp_server_name }}.name = {{ cookiecutter.smtp_server_name }}
{{ cookiecutter.smtp_server_name }}.host = {{ cookiecutter.smtp_server }}
{{ cookiecutter.smtp_server_name }}.queue_path = {{ cookiecutter.data_directory }}/mail

## PyAMS ZeroMQ TCP handlers
## These handlers are used by several application processes started by PyAMS application on startup.
## For each process, you can define:
## - the handler IP address (using "ip_address:port" syntax)
## - a boolean flag to specify if the matching process should be started; if you are not on the "main" application
##   process, the answer should be 'false'
## - a "login:password" authentication credentials to be used (between server and clients)
## - a list of allowed clients

{%- if cookiecutter.use_scheduler %}
## Tasks scheduler process settings
pyams_scheduler.tcp_handler = {{ cookiecutter.pyams_scheduler }}
pyams_scheduler.start_handler = {{ cookiecutter.start_scheduler }}
pyams_scheduler.allow_auth = {{ cookiecutter.scheduler_auth }}
pyams_scheduler.allow_clients = {{ cookiecutter.scheduler_clients }}

{%- endif %}
{%- if cookiecutter.use_medias_converter %}
## Medias converter process settings
pyams_media.tcp_handler = {{ cookiecutter.pyams_medias_converter }}
pyams_media.start_handler = {{ cookiecutter.start_medias_converter }}
pyams_media.allow_auth = {{ cookiecutter.converter_auth }}
pyams_media.allow_clients = {{ cookiecutter.converter_clients }}

{%- endif %}
{%- if cookiecutter.use_elasticsearch %}
## ElasticSearch server settings
pyams_elastic.servers =
    {{ cookiecutter.elasticsearch_server }}
;pyams_elastic.cloud_id = cloud_id
; you can set an API key defined by the key and it's secret, set "as is" or in base64 encoded form:
;pyams_elastic.api_key = api_key:key_secret
;pyams_elastic.api_key = base64.encode(api_key:key_secret)
; => pyams_elastic.api_key = VmdtZUFJMEJOMkF1UDdmcW5OS0g6X2Z3dEZmU1ZTVG1BbHRYVzdRSnJHQQ==
;pyams_elastic.basic_auth = username:password
;pyams_elastic.bearer_auth = bearer_token
pyams_elastic.use_ssl = {{ cookiecutter.elasticsearch_use_ssl }}
pyams_elastic.verify_certs = true
;pyams_elastic.ca_certs = /path/to/ca-certs.pem
;pyams_elastic.client_cert = /path/to/client-cert.pem
;pyams_elastic.client_key = /path/to/client.key
pyams_elastic.index = {{ cookiecutter.elasticsearch_index }}
pyams_elastic.timeout = 30
pyams_elastic.timeout_retries = 0
pyams_elastic.use_transaction = true
pyams_elastic.disable_indexing = false

{%- endif %}
{%- if cookiecutter.use_elasticsearch_indexer %}
## Elasticsearch indexer process settings
pyams_content_es.tcp_handler = {{ cookiecutter.pyams_elasticsearch_indexer }}
pyams_content_es.start_handler = {{ cookiecutter.start_elasticsearch_indexer }}
pyams_content_es.allow_auth = {{ cookiecutter.indexer_auth }}
pyams_content_es.allow_clients = {{ cookiecutter.indexer_clients }}

{%- endif %}
{%- if cookiecutter.use_chat %}
# PyAMS internal chat settings
pyams_chat.redis_server = {{ cookiecutter.chat_redis_db }}
pyams_chat.start_client = true
pyams_chat.channel_name = chat:{{ cookiecutter.project_slug }}
pyams_chat.notifications_key = chat:notifications
pyams_chat.rest_context_route = /api/chat/context
pyams_chat.jwt_refresh_route = /api/auth/jwt/token
pyams_chat.jwt_verify_route = /api/auth/jwt/verify
pyams_chat.ws_endpoint = ws://{{ cookiecutter.chat_ws_endpoint }}/ws/chat

{%- endif %}

## PyAMS content settings
pyams_content.lexicon.languages = {{ cookiecutter.lexicon_languages }}

# You can define your own tables and tools factories by overriding factory declaration
# using @factory_config, or by defining factory name for each tool.
# You can disable a table or tool by setting factory to 'off', 'none' or 'disabled'.

;pyams_content.config.references_manager_factory
;pyams_content.config.references_manager_name = references

;pyams_content.config.pictograms_table_factory = pyams_content.reference.pictogram.manager.PictogramManager
;pyams_content.config.pictograms_table_name = pictograms

;pyams_content.config.tools_manager_factory = pyams_content.shared.common.manager.SharedToolContainer
;pyams_content.config.tools_manager_name = tools

;pyams_content.config.views_tool_factory = pyams_content.shared.view.manager.ViewManager
;pyams_content.config.views_tool_name = views

;pyams_content.config.alerts_tool_factory = pyams_content.shared.alert.manager.AlertManager
;pyams_content.config.alerts_tool_name = views

;pyams_content.config.forms_tool_factory = pyams_content.shared.form.manager.FormManager
;pyams_content.config.forms_tool_name = views

;pyams_content.config.topics_tool_factory = pyams_content.shared.topic.manager.TopicManager
;pyams_content.config.topics_tool_name = topics


###
### uWSGI configuration
###

[uwsgi]
proj = {{ cookiecutter.project_slug }}
chdir = $((INSTALL))
processes = 8
threads = 2
offload-threads = 2
stats =  127.0.0.1:9191
max-requests = 100000
master = true
vacuum = true
enable-threads = true
harakiri = 60
chmod-socket = 644
plugin = python3
pidfile = %(chdir)/var/run/%(proj).pid
socket = 127.0.0.1:8080
buffer-size = 32768
virtualenv = %(chdir)
uid = {{ cookiecutter.run_user }}
gid = {{ cookiecutter.run_group }}
wsgi-file = %(chdir)/parts/wsgi/wsgi


###
### WSGI server configuration
###

[server:main]
use = egg:waitress#main
host = 0.0.0.0
port = {{ cookiecutter.webapp_port }}

max_request_body_size = 10737418240


###
### logging configuration
###

[loggers]
keys = root, {{ cookiecutter.project_slug }}

[handlers]
keys = console

[formatters]
keys = generic

[logger_root]
level = DEBUG
handlers = console

[logger_{{ cookiecutter.project_slug }}]
level = DEBUG
handlers =
qualname = {{ cookiecutter.project_slug }}

[handler_console]
class = StreamHandler
args = (sys.stderr,)
level = NOTSET
formatter = generic

[formatter_generic]
format = %(asctime)s %(levelname)-5.5s [%(name)s][%(threadName)s] %(message)s
