WSGIDaemonProcess {{ cookiecutter.project_slug }} user={{ cookiecutter.run_user }} group={{ cookiecutter.run_group }} processes=8 threads=2 display-name="{{ cookiecutter.project_name }}"

<VirtualHost *:80>

    ServerAdmin     webmaster@localhost
    ServerName      {{ cookiecutter.virtual_hostname }}

{%- if cookiecutter.use_https %}

    RewriteEngine   on
    RewriteRule     (.*)    https://{{ cookiecutter.virtual_hostname}}$1

</VirtualHost>

<VirtualHost *:443>

    ServerAdmin     webmaster@localhost
    ServerName      {{ cookiecutter.virtual_hostname }}
{%- endif %}

    DocumentRoot    {{ cookiecutter.data_directory }}/htdocs
    ErrorLog        {{ cookiecutter.logs_directory }}/{{ cookiecutter.webapp_name }}-error.log

    LogLevel        warn

    CustomLog       {{ cookiecutter.logs_directory }}/{{ cookiecutter.webapp_name }}-access.log combined

    WSGIScriptAlias     /   $((INSTALL))/parts/wsgi/wsgi
    WSGIImportScript        $((INSTALL))/parts/wsgi/wsgi process-group={{ cookiecutter.project_slug }} application-group=%{GLOBAL}
    WSGIPassAuthorization   on

    <Location />
        Require all granted

        WSGIProcessGroup        {{ cookiecutter.project_slug }}
        WSGIApplicationGroup    %{GLOBAL}
    </Location>

    <Directory $((INSTALL))/parts/wsgi>
        Require all granted
    </Directory>

{%- if cookiecutter.use_chat %}

    RewriteEngine   on

    RewriteCond     %{HTTP:Upgrade} =websocket [NC]
    RewriteRule     /(.*)           ws://{{ cookiecutter.chat_ws_endpoint }}/$1  [P,L]
{%- endif %}

{%- if cookiecutter.use_https %}

    SSLEngine       on
{%- if cookiecutter.use_chat %}
    SSLProxyEngine  on
{%- endif %}

    SSLCertificateFile      /etc/ssl/certs/{{ cookiecutter.virtual_hostname}}.pem
    SSLCertificateKeyFile   /etc/ssl/private/{{ cookiecutter.virtual_hostname}}.key
{%- endif %}

</VirtualHost>
