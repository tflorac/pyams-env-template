This directory contains Elasticsearch index and mappings settings.

To delete your previous index version:
- curl -XDELETE {{ cookiecutter.elasticsearch_server }}/{{ cookiecutter.elasticsearch_index }}

To initialize your index, just do:
- curl -XPUT {{ cookiecutter.elasticsearch_server }}/_ingest/pipeline/attachment -d @attachment-pipeline.json
- curl -XPUT {{ cookiecutter.elasticsearch_server }}/{{ cookiecutter.elasticsearch_index }} -d @index-settings.json
- curl -XPUT {{ cookiecutter.elasticsearch_server }}/{{ cookiecutter.elasticsearch_index }}/WfNewsEvent/_mapping -d @mappings/WfNewsEvent.json
- curl -XPUT {{ cookiecutter.elasticsearch_server }}/{{ cookiecutter.elasticsearch_index }}/WfBlogPost/_mapping -d @mappings/WfBlogPost.json
- curl -XPUT {{ cookiecutter.elasticsearch_server }}/{{ cookiecutter.elasticsearch_index }}/WfImageMap/_mapping -d @mappings/WfImageMap.json

To feed your index with previous contents:
- ./bin/pyams_es_index etc/production.ini
