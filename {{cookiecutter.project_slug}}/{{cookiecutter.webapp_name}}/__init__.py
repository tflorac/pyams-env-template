#
# Copyright (c) 2008-2015 Thierry Florac <tflorac AT ulthar.net>
# All Rights Reserved.
#
# This software is subject to the provisions of the Zope Public License,
# Version 2.1 (ZPL).  A copy of the ZPL should accompany this distribution.
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#

{%- if cookiecutter.db_type in ('relstorage', 'newt') %}
try:
    from psycogreen import gevent as psyco_gevent
except ImportError:
    psyco_gevent = None
{%- endif %}

from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid.csrf import CookieCSRFStoragePolicy
from pyramid.settings import asbool

from pyams_security.policy import PyAMSSecurityPolicy
from pyams_site.site import site_factory
from pyams_utils.request import PyAMSRequest


{%- if cookiecutter.db_type in ('relstorage', 'newt') %}
if psyco_gevent is not None:
    psyco_gevent.patch_psycopg()
{%- endif %}


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator()
    config.hook_zca()
    config.setup_registry(root_factory=site_factory,
                          request_factory=PyAMSRequest,
                          settings=settings)

    policy = PyAMSSecurityPolicy(secret=config.registry.settings.get(
                                     'pyams.security_policy.secret', 'PyAMS 0.1.0'),
                                 http_only=True,
                                 secure=asbool(config.registry.settings.get(
                                     'pyams.security_policy.secure', True)))
    config.set_security_policy(policy)

    config.set_csrf_storage_policy(
        CookieCSRFStoragePolicy(secure=asbool(config.registry.settings.get(
                                    'pyams.cookie_storage_policy.secure', True))))
    config.set_default_csrf_options(require_csrf=True)

    config.scan()

    return config.make_wsgi_app()
