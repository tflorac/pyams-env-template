import os
from setuptools import find_packages, setup


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'docs', 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'docs', 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
{%- if cookiecutter.use_oauth_auth %}
    'authomatic',
{%- endif %}
{%- if cookiecutter.use_oracle %}
    'cx_Oracle',
{%- endif %}
    'fanstatic',
{%- if cookiecutter.db_type in ('relstorage', 'newt') %}
    'mdtools.relstorage',
{%- endif %}
{%- if cookiecutter.db_type == 'newt' %}
    'newt.db',
{%- endif %}
{%- if cookiecutter.use_postgresql %}
    '{{ cookiecutter.db_driver }}',
{%- endif %}
    'pyams_alchemy',
{%- if cookiecutter.use_elasticsearch_apm %}
    'pyams_apm',
{%- endif %}
{%- if cookiecutter.use_apikey_auth %}
    'pyams_auth_apikey',
{%- endif %}
{%- if cookiecutter.use_azure_auth %}
    'pyams_auth_azure',
{%- endif %}
{%- if cookiecutter.use_http_auth %}
    'pyams_auth_http',
{%- endif %}
{%- if cookiecutter.use_jwt_auth %}
    'pyams_auth_jwt',
{%- endif %}
{%- if cookiecutter.use_ldap_auth %}
    'pyams_auth_ldap',
{%- endif %}
{%- if cookiecutter.use_oauth_auth %}
    'pyams_auth_oauth',
{%- endif %}
{%- if cookiecutter.use_sql_auth %}
    'pyams_auth_sql',
{%- endif %}
    'pyams_batching',
    'pyams_catalog',
{%- if cookiecutter.use_chat %}
    'pyams_chat',
{%- endif %}
{%- if cookiecutter.provide_content_cms %}
    'pyams_content',
{%- if cookiecutter.provide_content_api %}
    'pyams_content_api',
{%- endif %}
{%- if cookiecutter.use_elasticsearch %}
    'pyams_content_es',
{%- endif %}
    'pyams_content_themes',
{%- endif %}
{%- if cookiecutter.use_elasticsearch %}
    'pyams_elastic',
{%- endif %}
    'pyams_fields',
    'pyams_file',
    'pyams_file_views',
    'pyams_form',
    'pyams_i18n',
    'pyams_i18n_views',
    'pyams_layer',
    'pyams_mail',
    'pyams_pagelet',
    'pyams_portal',
{%- if cookiecutter.use_scheduler %}
    'pyams_scheduler',
{%- endif %}
    'pyams_security',
    'pyams_security_views',
    'pyams_sequence',
    'pyams_site',
    'pyams_skin',
    'pyams_table',
    'pyams_template',
    'pyams_thesaurus',
    'pyams_utils',
    'pyams_viewlet',
    'pyams_workflow',
    'pyams_zmi',
{%- if cookiecutter.use_scheduler %}
    'pyams_zmq',
{%- endif %}
    'pyams_zodb_browser',
{%- if cookiecutter.beaker_backend == 'memcached' %}
    'pylibmc',
{%- endif %}
    'pyramid',
    'pyramid_beaker',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_fanstatic',
    'pyramid_ipython',
    'pyramid_retry',
    'pyramid_tm',
    'pyramid_zcml',
    'pyramid_zodbconn',
{%- if cookiecutter.beaker_backend == 'ext:redis' %}
    'redis',
{%- endif %}
{%- if cookiecutter.db_type == 'relstorage' %}
    'relstorage',
{%- endif %}
    'transaction',
    'waitress',
    'WebTest',
{%- if cookiecutter.extension_package %}
    '{{ cookiecutter.extension_package }}',
{%- endif %}
    'ZODB',
    'zodbupdate',
    'zope.sqlalchemy'
]

setup(name='{{ cookiecutter.webapp_name }}',
      version='1.0',
      description='Pyramid project using PyAMS packages',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='Pyramid PyAMS application',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite='{{ cookiecutter.webapp_name }}',
      entry_points="""\
      [paste.app_factory]
      main = {{ cookiecutter.webapp_name }}:main
      """,
      )
